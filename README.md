# Kafka Demo
This project is a Spring Boot application that provides two APIs for interacting with Kafka and OpenSearch. It includes:

1. An API to produce events to Kafka.
2. An API to consume events from Kafka and push them to an OpenSearch Cloud cluster.


## APIs

### Produce API
- **URL**: `http://localhost:9090/produce`
- **HTTP Method**: PUT

### Request Parameters
- `topic` (required): The name of the topic

Make a PUT request to the /produce endpoint with the topic parameter set to the desired Kafka topic name. This API will read events from the Wikimedia recent changes stream and push them to the specified Kafka topic.

### Consume API

- **URL**: `http://localhost:9091/consume`
- **HTTP Method**: PUT

Make a PUT request to the /consume endpoint with the topic parameter set to the Kafka topic name you want to consume. This API will read records from the specified Kafka topic and push them to your OpenSearch Cloud cluster at the provided URL.
Example Request:
