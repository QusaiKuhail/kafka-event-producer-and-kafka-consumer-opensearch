package com.qusai.doemos.opensearch.kafkaconsumeropensearch.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.qusai.doemos.opensearch.kafkaconsumeropensearch.consumer.service.OpenSearchConsumerService;

@RestController
public class OpenSearchConsumerController {

	@Autowired
	OpenSearchConsumerService service;

	@PutMapping("/consume")
	public ResponseEntity<String> consumeToOpenSearch(@RequestParam String topic) {
		if (!service.getAllowedTopics().contains(topic)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Topic");
		}

		try {
			service.consume(topic);
			if (service.getCount() == 0) {
				return ResponseEntity.badRequest().body("No new events to consume and push to opensearch");
			}
			return ResponseEntity.ok(service.getCount() + " Messages Consumed Successfully to OpenSearch");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}

	}

}
