package com.qusai.demos.wikimedia.kafkaeventhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaEventHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaEventHandlerApplication.class, args);
	}

}
