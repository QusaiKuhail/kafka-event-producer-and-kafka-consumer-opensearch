package com.qusai.demos.wikimedia.kafkaeventhandler.producer.service.handler;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.MessageEvent;

public class WikimediaChangesHandler implements EventHandler {
	KafkaProducer<String, String> kafkaProducer;
	String topic;
	int counter = 0;
	private final Logger log = LoggerFactory.getLogger(WikimediaChangesHandler.class.getSimpleName());

	public WikimediaChangesHandler(KafkaProducer<String, String> producer, String topic) {
		this.kafkaProducer = producer;
		this.topic = topic;
	}

	@Override
	public void onOpen() {
		// nothing here

	}

	@Override
	public void onClosed() {
		kafkaProducer.close();

	}

	@Override
	public void onMessage(String event, MessageEvent messageEvent) {
		log.info(messageEvent.getData());
		// asynchronous code
		kafkaProducer.send(new ProducerRecord<>(topic, messageEvent.getData()));
		counter++;

	}

	@Override
	public void onComment(String comment) {
		// nothing here

	}

	@Override
	public void onError(Throwable t) {
		log.error("Error in stream Reading", t);

	}

	public int getCounter() {
		return counter;
	}

}
