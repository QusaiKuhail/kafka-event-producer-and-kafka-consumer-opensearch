package com.qusai.doemos.opensearch.kafkaconsumeropensearch.consumer.service;

import com.google.gson.JsonParser;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.opensearch.action.bulk.BulkRequest;
import org.opensearch.action.bulk.BulkResponse;
import org.opensearch.action.index.IndexRequest;
import org.opensearch.client.RequestOptions;
import org.opensearch.client.RestClient;
import org.opensearch.client.RestHighLevelClient;
import org.opensearch.client.indices.CreateIndexRequest;
import org.opensearch.client.indices.GetIndexRequest;
import org.opensearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Service
public class OpenSearchConsumerService {
	private static final Logger log = LoggerFactory.getLogger(OpenSearchConsumerService.class.getSimpleName());

	private Properties getProperties() {
		// Create Producer Properties
		Properties properties = new Properties();

		// Connect to the cluster
		try (InputStream inputStream = OpenSearchConsumerService.class.getClassLoader()
				.getResourceAsStream("kafka-config-consumer.properties")) {
			if (inputStream != null) {
				properties.load(inputStream);
			}
		} catch (IOException e) {
			log.error("Error while reading Kafka configurations" + e);
		}

		return properties;
	}

	public List<String> getAllowedTopics() {
		List<String> topicsList = new ArrayList<>();
		Properties properties = new Properties();

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("topics.properties")) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String allowedTopics = properties.getProperty("allowed.topics");

		if (allowedTopics != null) {
			String[] topics = allowedTopics.split(",");
			for (String topic : topics) {
				topicsList.add(topic.trim());
			}
		} else {
			log.error("No 'allowed.topics' property found in the properties file.");
		}

		return topicsList;
	}

	private RestHighLevelClient createOpenSearchClient() {
		// String connString = "http://localhost:9200";
		String connString = "https://7qx23d3rnh:ga4tz41c5i@qusai-kafka-4300190956.eu-central-1.bonsaisearch.net:443";
		// we build a URI from the connection string
		RestHighLevelClient restHighLevelClient;
		URI connUri = URI.create(connString);
		// extract login information if it exists
		String userInfo = connUri.getUserInfo();

		if (userInfo == null) {
			// REST client without security
			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(connUri.getHost(), connUri.getPort(), "http")));

		} else {
			// REST client with security
			String[] auth = userInfo.split(":");

			CredentialsProvider cp = new BasicCredentialsProvider();
			cp.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(auth[0], auth[1]));

			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(connUri.getHost(), connUri.getPort(), connUri.getScheme()))
							.setHttpClientConfigCallback(
									httpAsyncClientBuilder -> httpAsyncClientBuilder.setDefaultCredentialsProvider(cp)
											.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())));

		}

		return restHighLevelClient;
	}

	private String extractId(String value) {
		return JsonParser.parseString(value).getAsJsonObject().get("meta").getAsJsonObject().get("id").getAsString();
	}

	public int consume(String topic) throws IOException {
		// First create opensearch client
		RestHighLevelClient openSearchClient = createOpenSearchClient();
		Logger log = LoggerFactory.getLogger(OpenSearchConsumerService.class.getSimpleName());
		long startTime = System.currentTimeMillis();
		boolean timeFlag = true;
		int count = 0;

		// create our kafka client
		KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(getProperties());

		final Thread mainThread = Thread.currentThread();

		// adding the shutdown hool
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				log.info("Detected a shutdown,let's exit by calling consumer.wakeup()...");
				kafkaConsumer.wakeup();

				// join the main thread to allow the execution of the code in the main thread
				try {
					mainThread.join();
				} catch (InterruptedException e) {
					log.error("InterruptedException", e);
				}
			}
		});

		// We need to create the index on Opensearch if it doesn't exist already
		try (openSearchClient; kafkaConsumer) {
			boolean indexExists = openSearchClient.indices().exists(new GetIndexRequest("wikimedia"),
					RequestOptions.DEFAULT);
			if (!indexExists) {
				CreateIndexRequest createIndexRequest = new CreateIndexRequest("wikimedia");
				openSearchClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
				log.info("The Wikimedia index has been created");
			} else {
				log.info("The Wikimedia index already exits");
			}
			kafkaConsumer.subscribe(Collections.singleton("wikimedia.recentchange"));
			while (timeFlag) {
				ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(3000));
				int recordCount = records.count();
				log.info("Received " + recordCount + " record(s)");
				BulkRequest bulkRequest = new BulkRequest();
				for (ConsumerRecord<String, String> record : records) {
					// send the record into opensearch

					try {
						String id = extractId(record.value());
						IndexRequest indexRequest = new IndexRequest("wikimedia")
								.source(record.value(), XContentType.JSON).id(id);
						bulkRequest.add(indexRequest);
					} catch (Exception e) {
						log.error(e.getMessage());

					}

				}
				if (bulkRequest.numberOfActions() > 0) {
					BulkResponse bulkItemResponses = openSearchClient.bulk(bulkRequest, RequestOptions.DEFAULT);
					log.info("Inserted " + bulkItemResponses.getItems().length + " record(s)");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
				}
				if (System.currentTimeMillis() - startTime > 40000) {
					timeFlag = false;
					return count;
				}
			}

		} catch (WakeupException e) {
			log.info("Consumer is starting to shutdown");
		} catch (Exception e) {
			log.error("Unexpected exception in the consumer", e);
		} finally {
			kafkaConsumer.close();
			openSearchClient.close();
			log.info("The consumer is now gracefully shut down");
		}
	}

}
