package com.qusai.demos.wikimedia.kafkaeventhandler.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.qusai.demos.wikimedia.kafkaeventhandler.producer.service.WikimediaChangesService;

@RestController
public class ProducerController {

	@Autowired
	WikimediaChangesService service;
	
	@PutMapping("/produce")
	public ResponseEntity<String> produceEventMessages(@RequestParam String topic) throws InterruptedException{
		if (!service.getAllowedTopics().contains(topic)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Topic");
		}
		service.produce(topic);
		if(service.getCounter()==0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No new events to produce to the topic");
		}
		return ResponseEntity.ok(service.getCounter() +" Messages Produced Successfully");
	}
	
}
