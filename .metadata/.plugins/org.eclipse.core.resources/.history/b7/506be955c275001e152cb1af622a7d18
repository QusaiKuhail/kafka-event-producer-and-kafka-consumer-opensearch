package com.qusai.doemos.opensearch.kafkaconsumeropensearch.consumer.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.opensearch.client.RestClient;
import org.opensearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class OpenSearchConsumerService {
	private static final Logger log = LoggerFactory.getLogger(OpenSearchConsumerService.class.getSimpleName());

	private Properties getProperties() {
		// Create Producer Properties
		Properties properties = new Properties();

		// Connect to the cluster
		try (InputStream inputStream = OpenSearchConsumerService.class.getClassLoader()
				.getResourceAsStream("kafka-config-consumer.properties")) {
			if (inputStream != null) {
				properties.load(inputStream);
			}
		} catch (IOException e) {
			log.error("Error while reading Kafka configurations" + e);
		}

		return properties;
	}

	public List<String> getAllowedTopics() {
		List<String> topicsList = new ArrayList<>();
		Properties properties = new Properties();

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("topics.properties")) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String allowedTopics = properties.getProperty("allowed.topics");

		if (allowedTopics != null) {
			String[] topics = allowedTopics.split(",");
			for (String topic : topics) {
				topicsList.add(topic.trim());
			}
		} else {
			log.error("No 'allowed.topics' property found in the properties file.");
		}

		return topicsList;
	}

	private static RestHighLevelClient createOpenSearchClient() {
		// String connString = "http://localhost:9200";
		String connString = "https://7qx23d3rnh:ga4tz41c5i@qusai-kafka-4300190956.eu-central-1.bonsaisearch.net:443";
		// we build a URI from the connection string
		RestHighLevelClient restHighLevelClient;
		URI connUri = URI.create(connString);
		// extract login information if it exists
		String userInfo = connUri.getUserInfo();

		if (userInfo == null) {
			// REST client without security
			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(connUri.getHost(), connUri.getPort(), "http")));

		} else {
			// REST client with security
			String[] auth = userInfo.split(":");

			CredentialsProvider cp = new BasicCredentialsProvider();
			cp.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(auth[0], auth[1]));

			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(connUri.getHost(), connUri.getPort(), connUri.getScheme()))
							.setHttpClientConfigCallback(
									httpAsyncClientBuilder -> httpAsyncClientBuilder.setDefaultCredentialsProvider(cp)
											.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())));

		}

		return restHighLevelClient;
	}

}
