package com.qusai.demos.wikimedia.kafkaeventhandler.producer.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;

@Service
public class WikimediaChangesService {
	private static final Logger log = LoggerFactory.getLogger(WikimediaChangesService.class.getSimpleName());

	private Properties getProperties() {
		// Create Producer Properties
		Properties properties = new Properties();

		// Connect to the cluster
		try (InputStream inputStream = WikimediaChangesService.class.getClassLoader()
				.getResourceAsStream("kafka-config-producer.properties")) {
			if (inputStream != null) {
				properties.load(inputStream);
			}
		} catch (IOException e) {
			log.error("Error while reading Kafka configurations" + e);
		}

		return properties;
	}

	public int produce(String topic) throws InterruptedException {
		// Create The Producer'
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(getProperties());
		WikimediaChangesHandler wikimediaChangesHandler = new WikimediaChangesHandler(producer, topic);
		EventHandler eventHandler = wikimediaChangesHandler;
		String url = "https://stream.wikimedia.org/v2/stream/recentchange";
		EventSource.Builder builder = new EventSource.Builder(eventHandler, URI.create(url));
		EventSource eventSource = builder.build();

		// start the producer in another thread
		eventSource.start();

		// we produce for 10 seconds and blocks the program until then
		TimeUnit.SECONDS.sleep(10);

		return wikimediaChangesHandler.getCounter();

	}
	
	public List<String> getAllowedTopics() {
		List<String> topicsList = new ArrayList<>();
		Properties properties = new Properties();

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("topics.properties")) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String allowedTopics = properties.getProperty("allowed.topics");

		if (allowedTopics != null) {
			String[] topics = allowedTopics.split(",");
			for (String topic : topics) {
				topicsList.add(topic.trim());
			}
		} else {
			log.error("No 'allowed.topics' property found in the properties file.");
		}

		return topicsList;
	}
}
